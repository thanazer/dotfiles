#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# Pyenv stuff
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# NVM stuff
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# GHC stuff
[ -f "/home/aneeshd/.ghcup/env" ] && source "/home/aneeshd/.ghcup/env" # ghcup-env

# Go stuff
export PATH=$PATH:/usr/local/go/bin
export GOPATH=$HOME/golib/:$HOME/gosrc/

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

# opam configuration
test -r /home/aneeshd/.opam/opam-init/init.sh && . /home/aneeshd/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true
