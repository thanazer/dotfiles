;;; early-init.el --- The early startup configuration file
;;;
;;; Commentary: This configuration file is loaded and run before init.el.
;;; In this file, straight.el - the package manager - is bootstrapped.
;;; Along with straight, use-package macro is setup and some very early
;;; UI elements, which I absolutely do not want, are removed.
;;;
;;; Code:

;; Temporarily increase garbage collector threshold. This is purported to
;; improve loading times. This is reverted immediately after configuration
;; is loaded. Won't cause any harm.
(add-hook 'after-init-hook
	  `(lambda ()
	     (setq gc-cons-threshold ,gc-cons-threshold))
	  'append)
(setq gc-cons-threshold most-positive-fixnum)

(defun bootstrap-straight ()
  "Install and configure straight for the first time."
  (setq straight-vc-git-default-clone-depth 'full
	straight-check-for-modifications '(check-on-save find-when-checking)
	straight-build-dir (format "build-%s" emacs-version))
  (defvar bootstrap-version)
  (let ((bootstrap-file
	 (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
	(bootstrap-version 6))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
	(goto-char (point-max))
	(eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage)))

(defun bootstrap-use-package ()
  "Install and configure use-package macro."
  (setq package-enable-at-startup nil
	use-package-enable-imenu-support t)
  (straight-use-package 'use-package)
  (straight-use-package 'diminish))

(bootstrap-straight)
(bootstrap-use-package)

;; Disable all bars and inhibit the startup screen.
(dolist (mode '(scroll-bar-mode
		horizontal-scroll-bar-mode
		menu-bar-mode
		tool-bar-mode))
  (when (fboundp mode)
    (funcall mode 0)))
(setq inhibit-startup-screen t)

;; Also become fullscreen before doing anything else.
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;;; early-init.el ends here
