;;; 75-coding-lsp.el --- LSP/Langugae specific setups
;;;
;;; Commentary: This file sets up eglot for language servers and sets
;;; various needed settings for each language.
;;; Currently, my goal is to have emacs set up for the following languages:
;;; 1. JavaScript
;;; 2. TypeScript
;;; 3. Python
;;; 4. Go
;;; 5. C/C++
;;;
;;; Somewhere down the line, I also want emacs to be able to handle swift.
;;;
;;; Code:

(use-package eglot
  :straight (:type built-in)
  :defer t
  :commands
  (eglot eglot-ensure)
  :custom
  ((eglot-autoshutdown t)))

;; go-lang
(use-package go-ts-mode
  :straight (:type built-in)
  :mode "\\.go\\'"
  :init
  (setenv "GOPATH" "")
  :hook
  (go-ts-mode . (lambda ()
                  (eglot-ensure))))

;; python
(use-package python-mode
  :straight (:type built-in)
  :mode "\\.py\\'"
  :hook
  (python-mode . (lambda ()
                   (python-ts-mode)
                   (eglot-ensure))))

(use-package pyenv
  :straight (:host github :repo "aiguofer/pyenv.el")
  :mode "\\.py\\'"
  :config
  (global-pyenv-mode))

(use-package switch-buffer-functions
  :straight t
  :config
  ;; Look for .python-version in the directory and change version
  (defun pyenv-update-on-buffer-switch (prev curr)
    (if (string-equal "Python" (format-mode-line mode-name nil nil curr))
        (pyenv-use-corresponding)))

  (add-hook 'switch-buffer-functions 'pyenv-update-on-buffer-switch))

;; ocaml
(use-package tuareg
  :straight t
  :mode
  (("\\.ml\\'" . tuareg-mode)
   ("\\.mli\\'" . tuareg-mode)
   ("\\.mlp\\'" . tuareg-mode))
  :hook
  (tuareg-mode . eglot-ensure)
  :init
  (load-file (expand-file-name "vendor/ocp-indent.el" user-emacs-directory))
  (setq standard-indent 2)
  (setq tab-width 2)
  (setq indent-tabs-mode nil)
  (setq c-basic-offset 2)
  (setq smie-indent-basic 2))

;;; 75-coding-lsp.el ends here
