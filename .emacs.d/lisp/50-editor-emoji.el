;;; 50-editor-emoji.el --- Emoji support
;;;
;;; Commentary: Love being able to insert emoji in commit messages and such.
;;;
;;; Code:

;; Using Apple emoji font for emoji here.
;; TODO: Setup a way to configure the font based on OS.
(use-package emojify
  :straight t
  :defer t
  :commands
  emojify-insert-emoji
  :init
  (general-define-key
   "C-c ." '(emojify-insert-emoji :which-key "Insert emoji"))
  :config
  (when (member "Noto Color Emoji" (font-family-list))
    (set-fontset-font
     t 'symbol (font-spec :family "Noto Color Emoji") nil 'prepend))
  (setq emojify-display-style 'unicode)
  (setq emojify-emoji-styles '(unicode)))

;;; 50-editor-emoji.el ends here
