;;; 48-editor-sudo.el --- Sudo edit configuration
;;;
;;; Commentary: Setup sudo edit to be able to change permissions
;;; for a file even after opening.
;;;
;;; Code:

(use-package sudo-edit
  :straight t
  :defer t
  :commands
  (sudo-edit sudo-edit-find-file)
  :init
  (leader-keys
    "s" '(:ignore t :which-key "Sudo")
    "s e" '(sudo-edit :which-key "Edit")
    "s f" '(sudo-edit-find-file :which-key "Find file")))

;;; 48-editor-sudo.el ends here
