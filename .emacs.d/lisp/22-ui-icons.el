;;; 22-ui-icons.el --- Icons setup
;;;
;;; Commentary: Use all-the-icons for fancy file icons everywhere and
;;; all-the-icons-dired mode for icons in dired buffers.
;;;
;;; Code:

(use-package all-the-icons
  :straight t
  :demand
  :if
  (display-graphic-p))

(use-package all-the-icons-dired
  :straight t
  :defer t
  :hook
  (dired-mode . (lambda ()
		  (all-the-icons-dired-mode t))))

;;; 22-ui-icons.el ends here
