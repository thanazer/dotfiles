;;; 10-lib.el --- Utility functions
;;;
;;; Commentary: Various utility functions that are used throughout
;;; the configuration.
;;;
;;; Code:

;; This function comes in handy when a package is seemingly broken and I want to
;; rebuild and load it on the fly.
(defun thanazer/straight-pull-and-rebuild-package (package)
  "Pulls and rebuild the PACKAGE using straight."
  (interactive
   (list (straight--select-package "Pull and rebuild package"
				   #'straight--installed-and-buildable-p)))
  (straight-pull-package package)
  (straight-rebuild-package package))

(defun aneeshd/buffer-file-or-directory-name (buf)
  "The file BUF is visiting, works also if it's a `dired' buffer."
  (with-current-buffer buf
    (or buffer-file-name
        (and (eq major-mode 'dired-mode)
             (boundp 'dired-directory)
             (file-name-directory
              (if (stringp dired-directory)
                  dired-directory
                (car dired-directory)))))))

;;; 10-lib.el ends here
