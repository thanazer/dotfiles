;;; 21-ui-mini-frame.el --- Setup mini-frame popup
;;;
;;; Commentary: This sets up mini-frame which allows for the mini
;;; buffer to pop out.
;;; NOTE: NOT SURE HOW THIS WORKS WITH EGLOT.
;;;
;;; Code:

(use-package mini-frame
  :straight t
  :config
  (custom-set-variables
   '(mini-frame-show-parameters
     '((top . 10)
       (width . 0.7)
       (left . 0.5))))
  (mini-frame-mode))

;;; 21-ui-mini-frame.el ends here
