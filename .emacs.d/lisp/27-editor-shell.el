;;; 27-editor-shell.el --- Eshell setup
;;;
;;; Commentary: Setup for eshell which is my primary shell. Setup
;;; highlight, profile and aliases.
;;;
;;; Code:

(use-package eshell-toggle
  :straight t
  :defer t
  :commands
  (eshell-toggle)
  :config
  (setq eshell-toggle-size-fraction 2
        eshell-toggle-run-command nil
        eshell-toggle-init-function #'eshell-toggle-init-eshell))

(use-package eshell-syntax-highlighting
  :straight t
  :defer t
  :commands
  (eshell multi-eshell)
  :after
  esh-mode
  :init
  (eshell-syntax-highlighting-global-mode +1))

(use-package eshell
  :straight (:type built-in)
  :defer t
  :commands
  (eshell multi-eshell)
  :init
  (leader-keys
    "t e" '(eshell-toggle :which-key "Eshell"))

  (leader-keys
    "e" '(:ignore t :which-key "Eshell")
    "e n" '(multi-eshell :which-key "New eshell")
    "e s" '(multi-eshell-switch :which-key "Next eshell in ring"))
  :config
  (setq eshell-rc-script (expand-file-name "eshell/profile" user-emacs-directory)
        eshell-aliases-file (expand-file-name "eshell/aliases" user-emacs-directory)
        eshell-history-size 5000
        eshell-history-ignore-dups t
        eshell-scroll-to-bottom-on-input t
        eshell-destroy-buffer-when-process-dies t
        eshell-visual-commands '("fish" "ssh" "zsh"))

  ;; Use multi-eshell.el to create multiple eshells
  (require 'multi-eshell))

(use-package vterm
  :straight t
  :defer t
  :commands
  (vterm)
  :init
  (setq shell-file-name "/bin/bash"
        vterm-max-scrollback 5000))

(use-package vterm-toggle
  :straight t
  :defer t
  :commands
  (vterm-toggle)
  :after vterm
  :init
  (setq vterm-toggle-fullscreen-p nil)
  (setq vterm-toggle-scope 'project)
  (add-to-list 'display-buffer-alist
               '((lambda (buffer-or-name _)
                   (let ((buffer (get-buffer buffer-or-name)))
                     (with-current-buffer buffer
                       (or (equal major-mode 'vterm-mode)
                           (string-prefix-p vterm-buffer-name (buffer-name buffer))))))
                 (display-buffer-reuse-window display-buffer-at-bottom)
                 (reusable-frames . visible)
                 (window-height . 0.3))))

;;; 27-editor-shell.el ends here
