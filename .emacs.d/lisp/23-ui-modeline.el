;;; 23-ui-modeline.el --- Doom modeline setup
;;;
;;; Commentary: What is to say except that I need some bling?
;;;
;;; Code:

(use-package doom-modeline
  :straight t
  :demand
  :init (doom-modeline-mode 1))

;;; 23-ui-modeline.el ends here
