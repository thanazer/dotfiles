;;; 15-ui-theme.el --- Theme settings
;;;
;;; Commentary: Theme setup. I use doom emacs theme package in
;;; conjunction with doom-gruvbox-material-theme package to get
;;; the gruvbox-material theme I want.
;;;
;;; Code:

(use-package doom-themes
  :straight t
  :config
  ;; Enables bold and italics
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t)
  ;; Adds visual bell colors
  (doom-themes-visual-bell-config)
  ;; Adds org-mode colors
  (doom-themes-org-config)
  (load-theme 'doom-dark+ t))

;; (use-package doom-gruvbox-material-theme
;;   ;; Need to pull this package for github
;;   :straight (doom-gruvbox-material-theme
;;              :type git
;;              :host github
;;              :repo "Cardoso1994/doom-gruvbox-material-theme")
;;   ;; Need to load this package after doom-themes is loaded
;;   :after
;;   doom-themes
;;   :config
;;   ;; This package does not install anything directly. Need to add the install
;;   ;; folder to custom-theme-load-path and then load the theme.
;;   (add-to-list 'custom-theme-load-path
;;                (expand-file-name "straight/repos/doom-gruvbox-material-theme"
;;                                  user-emacs-directory))
;;   (setq doom-gruvbox-material-background "medium"
;;         doom-gruvbox-material-palette "mix")

;;   (setq doom-theme 'doom-gruvbox-material)
;;   (load-theme 'doom-gruvbox-material t))

;;; 15-ui-theme.el ends here
