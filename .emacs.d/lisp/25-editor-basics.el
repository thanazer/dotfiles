;;; 25-editor-basics.el --- Basic editor settings
;;;
;;; Commentary: Editor settings that do not belong to any package.
;;;
;;; Code:

;; Default indentation should be 2 spaces
(setq-default indent-tabs-mode nil)

;; Enable automatic pairs
(electric-pair-mode +1)

;;; 25-editor-basics.el ends here
