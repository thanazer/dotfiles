;;; 26-editor-treesit.el --- Treesitter setup
;;;
;;; Commentary: Now, treesitter is the best thing that has happened to editors in a
;;; while. Cannot live without the beautiful syntax highlighting.
;;; Treesitter is also available as a built-in package in Emacs 29.
;;; Need to install treesitter grammars, for which 'Mastering Emacs'
;;; has a good turotial: https://www.masteringemacs.org/article/how-to-get-started-tree-sitter.
;;;
;;; Code:

(use-package treesit
  :straight (:type built-in)
  :config
  ;; These are the language grammars I want installed if
  ;; not already present.
  (setq treesit-language-source-alist
	'((bash "https://github.com/tree-sitter/tree-sitter-bash")
	  (cmake "https://github.com/uyha/tree-sitter-cmake")
	  (css "https://github.com/tree-sitter/tree-sitter-css")
	  (elisp "https://github.com/Wilfred/tree-sitter-elisp")
	  (go "https://github.com/tree-sitter/tree-sitter-go")
    (gomod "https://github.com/camdencheek/tree-sitter-go-mod" "main" "src")
	  (html "https://github.com/tree-sitter/tree-sitter-html")
	  (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
	  (json "https://github.com/tree-sitter/tree-sitter-json")
	  (make "https://github.com/alemuller/tree-sitter-make")
	  (markdown "https://github.com/ikatyang/tree-sitter-markdown")
    (ocaml "https://github.com/tree-sitter/tree-sitter-ocaml" "master" "ocaml/src")
	  (python "https://github.com/tree-sitter/tree-sitter-python")
	  (toml "https://github.com/tree-sitter/tree-sitter-toml")
	  (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
	  (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
	  (yaml "https://github.com/ikatyang/tree-sitter-yaml")))

  (unless (file-exists-p (expand-file-name "tree-sitter" user-emacs-directory))
    (mapc #'treesit-install-language-grammar (mapcar #'car treesit-language-source-alist)))
  
  ;; Give me all the spaghetti
  (setq treesit-font-lock-level 4))

;;; 26-editor-treesit.el ends here
