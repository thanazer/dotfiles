;;; 11-keys-general.el --- General setup
;;;
;;; Commentary: Setup general for keybindings. Will probably be even more
;;; useful if I switch to using evil mode.
;;;
;;; Code:

(use-package general
  :straight t
  :config
  ;; Leader keys definer
  ;; This is used everywhere in the config to create keys for specific
  ;; package keymaps.
  (general-create-definer leader-keys
			  :prefix "C-c l")

  ;; global key definitions

  ;; Toggle keys (leader)
  (leader-keys
   "t" '(:ignore t :which-key "Toggle"))

  ;;Zoom
  (general-define-key
   "C-=" '(text-scale-increase :which-key "Zoom in")
   "C--" '(text-scale-decrease :which-key "Zoom out")))

;;; 11-keys-general.el ends here
