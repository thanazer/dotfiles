;;; 28-editor-vc.el --- Version control setup
;;;
;;; Commentary: Setup magit for git.
;;;
;;; Code:

(use-package magit
  :straight t
  :defer t
  :commands
  (magit-status))

(use-package git-gutter
  :straight t
  :defer t
  :hook
  (prog-mode . git-gutter-mode)
  :init
  (setq git-gutter:update-interval 0.02))

(use-package git-gutter-fringe
  :straight t
  :defer t
  :init
  (define-fringe-bitmap 'git-gutter-fr:added [224] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:modified [224] nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:deleted [128 192 224 240] nil nil 'bottom))

;;; 28-editor-vc.el ends here
