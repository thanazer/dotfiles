;;; 49-editor-org-mode.el --- Org-mode setup
;;;
;;; Commentary: This file configures all the packages that I use with
;;; org-mode.
;;;
;;; Code:

(use-package org
  :straight (:type built-in)
  :defer t
  :hook
  ;; Want proper indentation with org-mode
  (org-mode . org-indent-mode)
  :config
  ;; Fancy eliipsis
  (setq org-ellipsis " ▾"
        org-hide-emphasis-markers t)
  ;; Tempo gives a lot of good shortcuts for block
  ;; <s<TAB> for a source block
  (require 'org-tempo))

(use-package org-bullets
  :straight t
  :defer t
  :hook
  (org-mode . (lambda ()
		(org-bullets-mode 1))))

;;; 49-editor-org-mode.el ends here
