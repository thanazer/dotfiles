;;; init.el --- emacs init file
;;;
;;; Commentary: This file is the first that is loaded after the
;;; early-init.el. In this file, we only setup basic folders and
;;; no-littering package to keep the configuration folder tidy.
;;; This file also loads all the other configuration using config-lib.el.
;;;
;;; Code:

(use-package no-littering
  :straight t)

;; Everything inside the "vendor" directory is third-party code that
;; I use sometimes and have copied it from various places. This IMO
;; is a good way to organize random elisp. I can kill this folder
;; anytime without losing much.
(add-to-list 'load-path (expand-file-name "vendor/" user-emacs-directory))

;; Configuration changes through the customize UX are saved in a
;; separate file. Load this file if it is present.
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file 'noerror)

;; Load config-lib.el that manages loading the rest of the config.
;; The rest of the config is written in files of the format '10-xxx'
;; where the number indicates when the config file is loaded. Files with
;; smaller numbers are loaded first.
(require 'config-lib (expand-file-name "config-lib.el" user-emacs-directory))
(load-numbered-parts (expand-file-name "lisp/" user-emacs-directory))

;;; init.el ends here
